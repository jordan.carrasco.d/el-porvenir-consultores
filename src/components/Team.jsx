import React, {useState} from "react";
import Modal from "./modal"


export default (props) => {
  const[isVisible, setIsVisible] = useState(false)
  const[dataModal, setDataModal] = useState({})
  const openModal = (data) => {
    setIsVisible(true)
    setDataModal(data)
  }
  const closeModal = ()=> {
    setIsVisible(false)
  }
    return (
      <div id="team" className="text-center">
        <div className="container">
          <div className="col-md-8 col-md-offset-2 section-title">
            <h2>NUESTRO EQUIPO DE TRABAJO</h2>
            <p>
              Contamos con un equipo multidisciplinario.
            </p>
          </div>
          <div id="row">
            {props.data
              ? props.data.map((d, i) => (
                  <div  key={`${d.name}-${i}`} className="col-md-3 col-sm-6 team">
                    <div className="thumbnail">
                      {" "}
                      <img src={d.img} alt="..." className="team-img"/>
                      <div className="caption">
                        <h4>{d.name}</h4>
                        <p>{d.job}</p>
                        <button className="btn btn-lg btn-custom" onClick={e => openModal(d)}>info</button>
                      </div>
                    </div>
                  </div>
                ))
              : "loading"}
          </div>
        </div>
        <Modal isVisible={isVisible}
        onClose={closeModal}
        data={dataModal}
        />
      </div>
    );
  
}
