import React from "react";

export default (props) => {

    return (
      <div>
        <div id="contact">
          <div className="container">
            <div className="col-md-8">
              <div className="row">
                <div className="section-title">
                  <h2>Contáctanos</h2>
                  <div className="col-md-3 col-md-offset-1 contact-info">
                    <div className="contact-item">
                      <p>
                        <span>
                          <i className="fa fa-map-marker"></i> Dirección
                        </span>
                        {props.data ? props.data.address : "cargando"}
                      </p>
                    </div>
                    <div className="contact-item">
                      <p>
                        <span>
                          <i className="fa fa-phone"></i> Teléfono
                        </span>{" "}
                        <a href="tel:+56972430754">{props.data ? props.data.phone: "cargando"}</a>
                      </p>
                    </div>
                    <div className="contact-item">
                      <p>
                        <span>
                          <i className="fa fa-envelope-o"></i> Correo
                        </span>{" "}
                        <a href="mailto:contacto@elporvenirconsultores.cl">{props.data ? props.data.email : "loading"}</a>
                      </p>
                    </div>
                  </div>
                </div>
                
                
              </div>
            </div>
            {/*<div className="col-md-12">
              <div className="row">
                <div className="social">
                  <ul>
                    <li>
                      <a
                        href={props.data ? props.data.facebook : "/"}
                      >
                        <i className="fa fa-linkedin"></i>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>*/}
          </div>
        </div>
        <div id="footer">
          <div className="container text-center">
            <p>
              &copy; 2020 EL Porvenir Consultores.
            </p>
          </div>
        </div>
      </div>
    );
  
}
