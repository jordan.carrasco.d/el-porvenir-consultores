import React, { Component } from "react";

export class Header extends Component {
  render() {
    return (
      <header id="header">
        <div className="intro">
          <div className="overlay">
            <div className="container">
              <div className="row">
                <div className="col-md-8 col-md-offset-2 intro-text">
                  {/*<h1>
                    {this.props.data ? this.props.data.title : "Loading"}
                    <span></span>
                  </h1>*/}
                  <img src="img/logo-header.png" width="300" height="300" alt=""></img>
                  <p>
                    {this.props.data ? this.props.data.paragraph : "Loading"}
                  </p>
                  <a
                    href="#services"
                    className="btn btn-custom btn-lg page-scroll"
                  >
                    Saber más...
                  </a>{" "}
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
    );
  }
}

export default Header;
