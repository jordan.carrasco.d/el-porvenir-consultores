import React, {useState, useEffect} from 'react'

export default (props) => {
  const[isVisible, setIsVisible] = useState(false)
  useEffect(()=>{
    setIsVisible(props.isVisible)
  },[props.isVisible])
  const onClose = () => {
    props.onClose()
  }
    return isVisible && (
      <div className="wrapper">
        <div className="modal">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" onClick={onClose}>&times;</button>
              <h4 className="modal-title">{props.data.name}</h4>
              </div>
              <div className="modal-body">
                <p>{props.data.desc}</p>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-default" onClick={onClose}>Cerrar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  
}

